﻿using System.Collections;

namespace Taco.Core.Tests
{
    public static partial class AsyncExtensions
    {
        /// <summary>
        /// Unit Test를 위해 Awaiter를 IEnumerator로 전환하여주는 확장함수입니다. 이는 퍼포먼스 이슈가 있을 수 있으므로 테스트에서만 사용합니다.
        /// </summary>
        public static IEnumerator ToEnumerator<TResult>(this Task.AsyncExtensions.AsyncOperationAwaiter<TResult> self)
        {
            if (!self.IsCompleted)
                yield return null;
        }
    }
}