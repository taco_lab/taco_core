using UnityEngine;

namespace Taco.Core.Task
{
    public static partial class AsyncExtensions
    {
        public static AsyncOperationAwaiter<Object> GetAwaiter(this ResourceRequest resourceRequest)
        {
            Error.ThrowArgumentNullException(resourceRequest, nameof(resourceRequest));
            return new ResourceRequestAwaiter(resourceRequest);
        }

        class ResourceRequestAwaiter : AsyncOperationAwaiter<Object>
        {
            private readonly ResourceRequest _resourceRequest;

            public ResourceRequestAwaiter(ResourceRequest resourceRequest) : base(resourceRequest)
            {
                _resourceRequest = resourceRequest;
            }

            public override Object ResolveResult() => _resourceRequest.asset;
        }
    }
}