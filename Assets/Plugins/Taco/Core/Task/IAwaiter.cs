using System.Runtime.CompilerServices;

namespace Taco.Core.Task
{
    public interface IAwaiter : INotifyCompletion
    {
        bool IsCompleted { get; }
        void GetResult();
    }

    public interface IAwaiter<out TResult> : IAwaiter
    {
        new TResult GetResult();
    }
}