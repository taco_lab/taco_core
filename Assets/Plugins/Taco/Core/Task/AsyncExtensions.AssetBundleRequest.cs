using UnityEngine;

namespace Taco.Core.Task
{
    public static partial class AsyncExtensions
    {
        public static AsyncOperationAwaiter<Object> GetAwaiter(this AssetBundleRequest assetBundleRequest)
        {
            Error.ThrowArgumentNullException(assetBundleRequest, nameof(assetBundleRequest));
            return new AssetBundleRequestAwaiter(assetBundleRequest);
        }

        class AssetBundleRequestAwaiter : AsyncOperationAwaiter<Object>
        {
            private readonly AssetBundleRequest _assetBundleRequest;

            public AssetBundleRequestAwaiter(AssetBundleRequest assetBundleRequest) : base(assetBundleRequest)
            {
                _assetBundleRequest = assetBundleRequest;
            }

            public override Object ResolveResult() => _assetBundleRequest.asset;
        }
    }
}