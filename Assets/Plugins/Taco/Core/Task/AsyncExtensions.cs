using System;
using UnityEngine;

namespace Taco.Core.Task
{
    public static partial class AsyncExtensions
    {
        public abstract class AsyncOperationAwaiter<TResult> : IAwaiter<TResult>
        {
            private AsyncOperation _asyncOperation;

            // 이벤트 초기화를 위한 캡쳐
            private Action<AsyncOperation> _continuation;

            public AsyncOperationAwaiter(AsyncOperation asyncOperation)
            {
                _asyncOperation = asyncOperation;
            }

            public bool IsCompleted => _asyncOperation.isDone;

            public abstract TResult ResolveResult();

            public TResult GetResult()
            {
                var result = ResolveResult();
                if (_continuation != null)
                {
                    _asyncOperation.completed -= _continuation;
                    _asyncOperation = null;
                    _continuation = null;
                }

                return result;
            }


            void IAwaiter.GetResult() => GetResult();

            public void OnCompleted(Action continuation)
            {
                _continuation = (_) => continuation();
                _asyncOperation.completed += _continuation;
            }
        }
    }
}