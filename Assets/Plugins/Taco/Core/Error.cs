using System;

namespace Taco.Core
{
    public class Error
    {
        public static void ThrowArgumentNullException<TArgument>(TArgument argument, string argumentName)
            where TArgument : class
        {
            if (argument == null)
                throw new ArgumentNullException(argumentName);
        }
    }
}